#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

int
main (int argc, char **argv)
{
	unsigned int some_cond = rand ();
	GError *some_error = NULL;

	if (some_cond) {
		/* Bug: @some_error is not set on the
		 * FALSE path. */
		some_error = 
			g_error_new (G_FILE_ERROR,
			             G_FILE_ERROR_FAILED,
			             "bah!");
	}

	g_error_free (some_error);

	return 0;
}
