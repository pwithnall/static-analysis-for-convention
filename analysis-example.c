#include <stdlib.h>
#include <stdio.h>

typedef struct {
	const char *message;
} MyObject;

int
main (int argc, char **argv)
{
	MyObject *obj;
	unsigned int is_error;

	obj = malloc (sizeof (MyObject));

	if (argc == 1) {
		is_error = 1;
		free (obj);
	} else {
		is_error = 0;
		obj->message = argv[1];
	}

	if (is_error)
		puts (obj->message);

	return 0;
}
