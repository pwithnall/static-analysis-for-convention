\usepackage[english]{babel}
\usepackage{xcolor}
\usepackage{svg}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{ccicons}
\usepackage{textcomp}

% COLORS (Tango)
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

\lstset{
keywordstyle=[1]{\color{DarkSkyBlue}},
keywordstyle=[2]{\color{DarkScarletRed}},
keywordstyle=[3]{\bfseries},
keywordstyle=[4]{\color{DarkPlum}},
keywordstyle=[5]{\color{SkyBlue}},
commentstyle={\color{Aluminium4}},
stringstyle={\color{Chocolate}},
basicstyle={\ttfamily\small},
rulecolor=\color{black!30},
numbers=none,
numberstyle={\tiny},
language=C,
tabsize=4,
}

\title{Static analysis for convention}
\author{Philip Withnall}
\date{2014-11-06}


\begin{document}

\begin{frame}
\maketitle
\end{frame}

%===============================================================================
\section{Compilers 101}

\begin{frame}\frametitle{Compilers 101: What is Clang?}
\begin{description}
	\item[LLVM]{Compiler framework and standard intermediate language}
	\item[Clang]{\textbf{C-lang}uage front end}
	\item[Analyser]{Special operating mode of Clang}
\end{description}
\end{frame}

Let's start at the very basics, just to set the scene. Clang is a C compiler,
based on the LLVM compiler framework. LLVM is an infrastructure project,
defining an intermediate form of code, optimising it, and producing various
different outputs from it (such as assembly). On top, it has a number of
frontends, one of which is Clang, but others exist for other languages.

The Clang static analyser is a separate tool which is built from much the same
code as Clang, but which does not run at compile time. In this talk I'll
sometimes refer to the static analyser as simply `Clang' when the context makes
it clear.

\begin{frame}\frametitle{Compilers 101: What is Clang?}
\begin{itemize}
	\item{\underline{Not} GCC}
	\item{Easier development of plugins}
	\item{Non-copyleft licence (UoI/NCSA OSL)}
	\item{Active development community}
\end{itemize}
\end{frame}

As a compiler, one of the bigger features of Clang is that it is not GCC. It's a
much younger project, with an arguably more active development community. Clang
has stable, maturing support for most of the functionality which GCC has, and
often compiles things faster and with lower memory usage, but GCC still produces
faster binaries in many situations. The gap is narrowing, though, and one of the
things which makes Clang so attractive is that, as it was built from the ground
up as a series of libraries, reusing and extending its code to do interesting
new things is a lot easier than with GCC. GCC was originally explicitly built to
not be a library, or reusable
(\url{http://www.gnu.org/licenses/gcc-exception-3.1-faq.html}).

A couple of negative points about Clang and LLVM: they are both under a
non-copyleft licence, the University of Illinois/NCSA Open Source License
(\url{http://opensource.org/licenses/NCSA}). Development of both is driven to a
large part by Apple, for better or worse. However, this is not a talk about
politics.

%===============================================================================
\section{Compiler theory 101}

\begin{frame}\frametitle{Compiler theory 101: What is static analysis?}
Checking \emph{all} code paths with \emph{all} possible inputs, rather than the
ones used by a unit test

\vspace{0.5cm}

\begin{description}[Not so good for]
	\item[Opposed to]{dynamic analysis (unit testing)}
	\item[Good for]{error conditions, unusual data inputs}
	\item[Not so good for]{speed, checking for specific regressions}
\end{description}
\end{frame}

What is static analysis? It will be familiar to most of you, but let's briefly
run through the key points. A static analysis, as opposed to a dynamic analysis
(or unit test) is performed at compile time, not run time. It examines every
possible code path with all possible data inputs (within certain limitations),
rather than the limited set of code paths and data inputs exercised by a unit
test. This imposes a big cost in analysis time and memory, but can be impressive
at catching bugs on rarely-tested code paths (which unit tests almost
universally miss), or bugs arising from unusual data inputs (which are typically
security bugs).

\begin{frame}\frametitle{Compiler theory 101: Path-dependent analysis}
\begin{itemize}
	\item{Perform analysis on each control flow path in a program}
	\item{Maintain a set of constraints on each variable}
	\item{Maintain a model of memory}
	\item{All simplifying abstractions: \underline{precision is lost}}
\end{itemize}
\end{frame}

How does Clang analyse every code path? Broadly speaking, it walks through the
code in order, branching into two paths at every decision point, such as an
\lstinline{if} statement or \lstinline{while} condition. Each of these forms a
path.

Each path ends up being a series of program states, with each program state
being associated with a particular position in the code, a set of constraints in
place at that point, and a model of the memory state at that point.

Each time a decision point is reached and a new path is branched, a constraint
is added to each of the new branches, based on the decision. For example, the
two branches corresponding to an \lstinline{if} statement would have constraints
added for the \lstinline{if}-condition being true and being false. For a
\lstinline{while} loop, one branch would be constrained to the
\lstinline{while}-condition being true (and its path would then proceed into the
\lstinline{while} block), and the other to the condition being false (and its
path would proceed \emph{after} the block).

\begin{frame}[fragile]\frametitle{Compiler theory 101: Path-dependent analysis}
\lstinputlisting[firstline=7,lastline=23,gobble=8]{analysis-basics.c}
\end{frame}

\begin{frame}[fragile]\frametitle{Compiler theory 101: Path-dependent analysis}
\mode<article>{Try running these commands and playing with the resulting graph:}

\begin{centering}
	\mode<beamer>{{\Huge{Intermission}}\\
	\vspace{0.5cm}
	For playing with dotty
	\vspace{0.5cm}}

	\begin{lstlisting}[language=Bash,upquote=true]
	system_includes=`echo | cpp -Wp,-v 2>&1 | \
	    grep '^[[:space:]]' | \
	    sed -e 's/^[[:space:]]*/-isystem/' | \
	    tr "\n" ' '`
	clang -cc1 -analyze \
	    -analyzer-checker=debug.ViewExplodedGraph \
	    $system_includes analysis-basics.c
	\end{lstlisting}
\end{centering}
\end{frame}

By building up these constraints, bugs can be found. For example, if a path has
the constraint \lstinline{var_a == 15}, and then encounters an
\lstinline{if}-statement condition of \lstinline{var_a < 10}, it can solve the
two constraints and determine that the \lstinline{if} branch will never be
taken, so it can warn the user about unreachable code. This type of analysis is
most useful with nullability checks on variables, and helps flag potential
\lstinline{NULL} pointer dereferences. For example, if a path has the constraint
that \lstinline{my_str == NULL}, perhaps because the path is on the false
branch of an \lstinline{if}-statement checking nullability of
\lstinline{my_str}, and it reaches an expression which dereferences
\lstinline{my_str}, a bug has been found.

Two things complicate matters: analysis across compilation units, and function
pointers. Clang can only perform inter-procedural analysis (extending code paths
across function calls) if it has access to the code of the called function. This
is not possible in cases where a project is split up into multiple libraries,
for example. Because of the paucity of metadata on C interfaces, there isn't
much the analyser can do in this case except type analysis (which the compiler
already does on external function calls). It cannot, for example, associate
buffers with their length parameters, or check parameters which are dependent
on flag variables (such as \lstinline{open(O_CREAT)}).

Similarly, function pointers cause problems, since modelling them requires
modelling the data flow of the pointer variable --- where it's set, and to what
values. This is typically not an intractable problem, as function pointers are
almost always trivially set to a function (rather than the result of some
calculation or address arithmetic), and can be handled by the analyser's memory
model.

As well as tracking constraints on local variables, the Clang analyser tracks
the state of memory. It treats memory as a series of regions, starting with the
heap, stack, etc., and then producing subregions as the code being analysed
dictates. For example, it produces a new typed subregion for each stack variable
on entry to a function, or for each heap allocation. These regions have symbolic
addresses, but that rarely matters in analysis. They may have known or symbolic
sizes too, so that the result of \lstinline{malloc(some_computed_value)} can be
modelled.

The data stored in regions is tracked by means of a symbolic value bound to each
region. As with symbolic values in the rest of the analyser, a symbolic value
may be a constant, a known value such as a function address, a computed symbolic
value (such as `symbolic value X multiplied by 2'), an explicitly unknown value,
a symbolic value pointing to another memory region, etc. On an assignment to a
variable (or other memory region), the symbolic value associated with the memory
region is updated according to the assignment rvalue. So when a function pointer
is assigned to, a symbolic value pointing to the assigned function address is
associated with the memory region for that function pointer in the memory model
for the rest of the program states in the analysis of that code path.

\begin{frame}\frametitle{Path-dependent analysis example}
\lstinputlisting[firstline=11,lastline=25]{analysis-example.c}
\end{frame}

Here's an example; see if you can spot the errors. Our programmer has forgotten
to free \lstinline{obj} in the \lstinline{argc != 1} case, and has got the
wrong sense on the \lstinline{if}-condition at the bottom so it accesses
\lstinline{obj->message} after \lstinline{obj} has been freed.

\begin{frame}[fragile]\frametitle{Path-dependent analysis example}
\begin{lstlisting}[language=Bash]
$ gcc -Wall -Werror analysis-example.c
$ echo $?
0
\end{lstlisting}

\begin{lstlisting}[language=Bash]
$ clang -Wall -Werror analysis-example.c
$ echo $?
0
\end{lstlisting}
\end{frame}

Do compilers catch this? No, neither GCC or Clang do, even with all warnings
turned on.

\begin{frame}[fragile]\frametitle{Path-dependent analysis example}
\begin{lstlisting}[language=Bash]
$ clang --analyze -Wall -Werror analysis-example.c
analysis-example.c:25:9: warning: Use of memory
    after it is freed
                puts (obj->message);
                      ^~~~~~~~~~~~
analysis-example.c:27:9: warning: Potential leak
    of memory pointed to by 'obj'
        return 0;
               ^
2 warnings generated.
\end{lstlisting}
\end{frame}

However, Clang's static analyser catches both errors (passing the
\lstinline[language=Bash]{--analyze} option to Clang). How does it do that?

\begin{frame}\frametitle{Compiler theory 101: The war on false positives}
\begin{itemize}
	\item{Arise on mismatches between analysis model and runtime model}
	\item{i.e.\ When too much information is lost by the analysis}
	\item{Conflict between analysis complexity and speed}
	\item{\dots{}and forgetting things}
\end{itemize}
\end{frame}

False positives are the enemy of adoption of static analysis, and are an
unavoidable result of the mismatch between the analysis model, and the runtime
model. By making various simplifying assumptions in the analysis model,
information is lost, and when program execution depends on this information,
the analyser may produce false positives.

The most common situation where this can happen is on API boundaries, where
restrictions on incoming values are documented but not made obvious to the
compiler --- the kind of situation where the library author says `if you don't
adhere to the API documentation, the library behaviour is undefined' but the
analyser still tries to define and model its behaviour. For example, API
conventions where pointers passed into a function are always
non-\lstinline{NULL}, but the programmer hasn't used the \lstinline{nonnull}
attribute. The static analyser will produce warnings about potential
\lstinline{NULL} pointer dereferences.

Many false positives can be eliminated by making the analyser a little cleverer,
and in many cases they arise just because the analyser author hasn't thought of
a particular corner case. However, there is an inherent conflict between making
the analysis detailed and making it fast.

\begin{frame}[fragile]\frametitle{Compiler theory 101: The war on false positives}
\begin{lstlisting}
void *
non_failable_malloc (size_t size)
{
    void *out = NULL;

    assert (size > 0);

    /* Do the allocation, assign to out,
     * jump to done. */

done:
    assert (out != NULL);

    return out;
}
\end{lstlisting}
\end{frame}

How can we fix these? By making the analysis cleverer, but also by being more
explicit and less `clever' in our programming style. What helps a lot is to make
all assumptions explicit using assertions. If a function only operates correctly
when its inputs are in a certain domain (e.g.\ an integer has to be non-zero),
add that as a pre-condition assertion. If there are restrictions on its outputs,
add these as post-condition assertions. Doing this in C is a little ugly, and
requires coalescing all output paths into a single label and \lstinline{return}
statement, but it works well.

By doing this, you give the static analyser more information: for each
assertion, it can add a constraint to its analysis for that variable. For
pre-conditions these constraints will prevent false positives and catch
redundant checks inside the function. For post-conditions, they will catch code
paths where the output doesn't match the function's documentation. For example,
this can be used to check the common GLib idiom that a function either returns
a non-\lstinline{NULL} value, or sets its \lstinline{GError**} output parameter,
always doing exactly one of those.

As will be seen later, it is the use of programming conventions like explicitly
declaring function pre- and post-conditions using \lstinline{g_assert()} which
can make the difference between tractability and impossibility for static
analysis.

%===============================================================================
\section{Practical use of Clang's analyser}

\begin{frame}\frametitle{Using Clang to find bugs}
\begin{itemize}
	\item{Stand-alone usage with \texttt{--analyze}}
	\item{\lstinline[language=Bash]{scan-build}}
	\item{Integration with JHBuild}
\end{itemize}
\end{frame}

One of the trickier bits of static analysis is tooling and making sure that the
analysis is run regularly enough to keep on top of it. The best approach so far
is to run a continuous integration server which performs static analysis
regularly and makes the results available easily, while also allowing persistent
ignoring of known false positives. Developers are turned off by wading through
false positives, especially repeatedly --- just as with \texttt{-Wall}.

GNOME's JHBuild has a \lstinline[language=Python]{static_analyzer} configuration
variable --- just set it to \lstinline[language=Python]{True} and analysis will
be performed for every build, and the results persisted as HTML reports
(\url{http://www.murrayc.com/permalink/2013/11/15/jhbuild-and-clangs-scan-build/}).

Underneath, this uses the useful \lstinline[language=Bash]{scan-build} tool
which ships with Clang. It's a wrapper script which you run
\lstinline[language=Bash]{configure} and \lstinline[language=Bash]{make} under,
and it transparently sets the compiler to Clang, enables the analyser to run in
parallel with compilation, and saves the results as HTML reports.

\begin{frame}\frametitle{API-specific checkers}
\begin{itemize}
	\item{\lstinline[language=C]{printf()} format string checking on
	      steroids}
	\item{Checkers available for various stdlib and OS~X
	      APIs:\begin{itemize}
		\item{Unix API checker}
		\item{\lstinline{malloc()} \lstinline{sizeof} checker}
		\item{\lstinline{chroot()} checker}
		\item{\lstinline{malloc()} checker}
		\item{Cast to struct checker}
		\item{pthread lock checker}
	\end{itemize}}
\end{itemize}
\end{frame}

One of the key features of Clang is its plugin infrastructure, where every
analysis is performed by a separate \emph{checker}, which can be built in or
provided by a plugin. Plugins allow Clang to easily perform analysis specific to
third-party APIs. A basic example of this -- and for some reason pretty much the
only one which has made it into mainstream compilers -- is \lstinline{printf()}
format string checking. Quite why basic analysis of use of other stdlib
functions has not been implemented in GCC or Clang, I don't know, because many
of the analyses performed by Clang are not path-dependent, and hence could
easily be included in the main compiler.

It's these API-specific checks, which fill gaps in the machine-readable
semantics of functions, which provide one of the real features of static
analysis, and probably many of the more important fixes which they trigger.
Tracking side effects is one of the more valuable checks, where the analyser can
have some knowledge of the global state modified by, e.g.\ \lstinline{malloc()}
or \lstinline{open()} and track that to detect leaks.

Standard checkers include:
\begin{description}
	\item[Unix API checker]{
		Require a third argument when the \lstinline{O_CREAT} flag is
		specified; check for zero-sized \lstinline{malloc()} calls.
	}
	\item[\texttt{malloc()} \texttt{sizeof} checker]{
		Check for mismatches between \lstinline{sizeof} operators inside
		a \lstinline{malloc()} call and the type cast applied to the
		allocated result.
	}
	\item[\texttt{chroot()} checker]{
		Check for a \lstinline{chdir("/")} call immediately after a
		\lstinline{chroot()}.
	}
	\item[\texttt{malloc()} checker]{
		Check for double-free, leak and use-after-free for memory
		allocated with stdlib allocators.
	}
	\item[Cast to struct checker]{
		Disallow casts from non-\lstinline{void*} pointers to struct
		pointers.
	}
	\item[pthread lock checker]{
		Check for double lock, double unlock, invalid lock destruction,
		invalid lock initialisation and locking order reversal bugs with
		standard pthread locking functions.
	}
\end{description}

%===============================================================================
\section{Extending API-specific checks with Tartan}

\begin{frame}\frametitle{More API-specific checkers}
\begin{itemize}
	\item{Standard checkers only support stdlib and OS~X APIs}
	\item{What about GLib? GObject? libbadger?}
	\item{So: Write a custom Clang checker}
\end{itemize}
\end{frame}

So what about the APIs we use regularly? GLib, GObject, GStreamer? Your own pet
project? Clang cannot support these by default. However, you can use its plugin
architecture to add an API-specific checker.

Tartan (\url{http://people.collabora.com/~pwith/tartan/}) is just that: a Clang
plugin which adds checkers for GLib APIs.

\begin{frame}\frametitle{Tartan}
\begin{description}
	\item[Annotaters]{GIR attributes, \lstinline{g_assert()} preconditions}
	\item[Checkers]{Nullability, \lstinline{GVariant}, \lstinline{GSignal},
	                GIR attributes}
\end{description}
\end{frame}

It supports checking various things so far, such as whether the nullability of
function parameters matches the \lstinline{g_assert()} preconditions on the
function, whether parameters to \lstinline{GVariant} calls match the variant's
type string, whether \lstinline{GSignal} emission parameters match the signal's
definition, and it will soon support checking whether \lstinline{GError}s are
set correctly.

It is the use of conventions of using these APIs which makes it possible to use
them for static analysis. For example, \lstinline{GError} is just a structure
containing a message, code and domain. However, it has a strong convention of
being passed in to a function by reference, and being set by the function
exactly when it returns a failure value (e.g.\ \lstinline{NULL}) as its main
return value. By assuming this convention is uniformly used, Tartan can insert
constraints on function calls to unknown functions which take \lstinline{GError}
pointers --- namely, a constraint that the \lstinline{GError} pointer will be
\lstinline{NULL} exactly when the function's return value is
non-\lstinline{NULL}. This can reduce false positives for nullability checking
in the calling code, amongst other things.

\begin{frame}\frametitle{Basic operation}
\begin{itemize}
	\item{Annotations are added to code from metadata}
	\item{Checkers look for annotations and specific API calls}
\end{itemize}
\end{frame}

Tartan has two kinds of object: annotaters and checkers. Annotaters look at
metadata sources such as function preconditions and GIR files, and modify
Clang's abstract syntax tree (AST) to add attributes based on these assertions,
such as \lstinline{__attribute__((nonnull))}. These can then feed into all of
Clang's analyses (both those provided by Tartan, and existing nullability
analyses).

Checkers examine the AST or perform path-dependent analysis to find specific
bugs. Each checker focuses on a specific class of bug (e.g.\ \lstinline{GSignal}
emission bugs).

\begin{frame}[fragile]\frametitle{Tartan example}
\lstinputlisting[firstline=8,lastline=20]{tartan.c}
\end{frame}

\begin{frame}[fragile]\frametitle{Tartan example}
\begin{lstlisting}[language=Bash]
$ tartan -cc1 -analyze $system_includes \
    `pkg-config --cflags glib-2.0` tartan.c
tartan.c:16:2: warning: Freeing non-set GError
        g_error_free (some_error);
        ^~~~~~~~~~~~~~~~~~~~~~~~~
1 warning generated.
\end{lstlisting}
\end{frame}

Tartan can be run similarly to \lstinline[language=Bash]{scan-build}, as a
compiler wrapper. The various ways of running it are explained in detail on the
website: \url{http://people.collabora.com/~pwith/tartan/#usage}.

In this example, it has used a trivial path-dependent analysis to determine that
the \lstinline{GError} is only set on one control path out of two, but then
\lstinline{g_error_free()} is called on both, and will abort when passed
\lstinline{NULL}.

\begin{frame}\frametitle{Future plans}
\begin{itemize}
	\item{More user friendliness}
	\item{More checkers}
	\item{$\mathrm{usefulness} = \mathrm{users} \times \mathrm{checkers}$}
	\item{Use C attributes more instead of hard-coding APIs and constraints}
\end{itemize}
\end{frame}

A developer tool is only as useful as the product of the number of users and the
number of things it checks. At the moment, Tartan has very few users, and a
small number of checkers, so the plan is to concentrate equally on ease of use
and adding more checks.

Ease of use comes from:
\begin{itemize}
	\item{Eliminating false positives, either by increasing the accuracy of
	      the analysis, or by letting the user permanently mark an error as
	      a false positive.}
	\item{A gentle introduction to the tool, by allowing a user to slowly
	      enable new classes of errors and eliminate them, rather than
	      dumping hundreds of errors on the user to begin with.}
	\item{High-quality errors and depth of checks. If the user doesn't
	      understand an error message or what caused it, they will give up.}
	\item{Not requiring modifications to the code being analysed for false
	      positives. It makes the code ugly and increases the maintenance
	      burden.}
\end{itemize}

The checks which are planned to be performed in Tartan in future, such
as reference count checking of \lstinline{GObject} instances, should not be
siloed within Tartan. Reference counting is not a concept specific to GObject,
and it should be possible to introduce new C attributes to annotate function
parameters to indicate reference transfers; then the analysis could easily apply
to any library API which does reference counting (and which adds these
attributes). Similar generalisations could be applied to other Tartan checkers
for everyone's benefit.

%===============================================================================
\section{Coverity}

\begin{frame}[fragile]\frametitle{Briefly: Coverity}
\begin{itemize}
	\item{Popular static analysis system}
	\item{Free to use for FOSS projects (only)}
	\item{Reasonable sensitivity, high specificity}
	\item{Good tools for permanently ignoring false positives}
\end{itemize}
\end{frame}

Finally, Coverity should be mentioned. Most developers will have heard of it or
used it before (if you haven't, you should register any projects you maintain
on it right now --- it's really useful). It's free to use for FOSS projects (but
nothing closed source, unless you pay them). It catches quite a few problems
(API-specific and general) which Clang doesn't, and has a reasonably low false
positive rate. Even better, it has good tools for permanently ignoring false
positives.

%===============================================================================

\begin{frame}\frametitle{Miscellany}
\begin{description}[l]
	\item[LLVM/Clang/Analyser]{\hfill\begin{itemize}
		\item{\url{http://llvm.org/}}
		\item{\url{http://clang.llvm.org/}}
		\item{\url{http://clang-analyzer.llvm.org/}}
	\end{itemize}}
	\item[Tartan]{\url{http://people.collabora.com/~pwith/tartan/}}
	\item[Building a Checker in 24 hours]{
		\url{http://www.llvm.org/devmtg/2012-11/\#talk13} (slides at the top)
	}
	\item[Coverity]{\url{http://scan.coverity.com/}}
\end{description}

\vspace{0.6cm}

\begin{center}
\ccbysa
\\\vspace{0.05cm}
{\tiny{CC-BY-SA 4.0}}
\end{center}
\end{frame}

\end{document}
